﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Service.Entity;
using System.Data;
using System.Data.Common;
using System.Transactions;

namespace LiasIvmeService.Controllers
{
    [AllowAnonymous]
    [ApiController]
    [Authorize]
    public class IvmeController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private ILoginService _userService;

        public IvmeController(ILoginService kullaniciService, IConfiguration configuration)
        {
            _userService = kullaniciService;
            _configuration = configuration;
        }

        [HttpPost("Authenticate")]
        [AllowAnonymous]
        public IActionResult Authenticate(Kullanici model)
        {
            var response = _userService.Authenticate(model);

            if (response is null)
            {
                return Unauthorized(new { message = "Kullanici kodu veya şifre yanlış" });
            }

            return Ok(response);
        }
     
        [HttpGet]
        [Route("Ivme/IvmeOlayListesiGetir/{_tarih:DateTime}")]
        public IActionResult IvmeOlayListesiGetir(DateTime _tarih)
        {
            var response = new Operations(_configuration).IvmeOlayListesiGetir(_tarih);

            return Ok(response); 
        }       
    }
}
