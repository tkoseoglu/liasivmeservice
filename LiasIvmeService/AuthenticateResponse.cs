﻿using Service.Entity;

namespace LiasIvmeService
{
    public class AuthenticateResponse
    {
        public int Id { get; set; }
        public string KODU { get; set; }
        public string Token { get; set; }

        public AuthenticateResponse(Kullanici user, string token)
        {
            Id = Convert.ToInt32(user.KULLANICIID);
            KODU = user.KODU;
            Token = token;
        }
    }
}
