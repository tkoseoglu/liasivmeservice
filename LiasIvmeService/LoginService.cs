﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Service.Entity;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace LiasIvmeService
{
    public interface ILoginService
    {
        AuthenticateResponse Authenticate(Kullanici model);

        Kullanici GetKullaniciById(int id);
    }
    public class LoginService : ILoginService
    {
        private readonly AppSettings _appSettings;
        private readonly IConfiguration _configuration;

        public LoginService(IOptions<AppSettings> appSettings , IConfiguration configuration)
        {
            _appSettings = appSettings.Value;
            _configuration = configuration;
        }

        public AuthenticateResponse Authenticate(Kullanici model)
        {
            Kullanici user = new Operations(_configuration).getKullanici(model.KODU, model.SIFRESI);

            if (user is null)
            {
                return null;
            }

            var token = generateJwtToken(user);

            return new AuthenticateResponse(user, token);
        }

        public Kullanici GetKullaniciById(int id)
        {
             return new Operations(_configuration).getKullaniciById(id);
        }

        // helper methods

        private string generateJwtToken(Kullanici user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                new Claim("kullaniciId",user.KULLANICIID.ToString()),
                new Claim("loginTimeAndDate",DateTime.Now.ToString(CultureInfo.InvariantCulture)),
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
