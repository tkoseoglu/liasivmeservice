﻿using System.Data.Common;
using System.Data;
using Service.Entity.Model;
using System.Data.SqlClient;
using Service.Entity;
using Microsoft.Extensions.Configuration;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace LiasIvmeService
{
    public class Operations
    {
        private readonly IConfiguration _configuration;

        public Operations(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        #region Members

        DataTable dataTableIvmeOlay, dataTableIvmeOlayTuru, dataTableIvmeBulgu, dataTableIvmeKullanici;
        IvmeGeneralModel _ivmeGeneralModel;
        List<IvmeGeneralModel> _ivmeGeneralModels;

        #endregion

        #region Methods

        public List<IvmeGeneralModel> IvmeOlayListesiGetir(DateTime dateTime)
        {
            _ivmeGeneralModels = new List<IvmeGeneralModel>();
            dataTableIvmeOlay = new DataTable();

            dataTableIvmeOlay = ExecuteSQLDataTable(IvmeOlay(dateTime.ToString("yyyy-MM-dd") + " 00:00:00.000", dateTime.ToString("yyyy-MM-dd") + " 23:59:59.000"));

            foreach (DataRow dr in dataTableIvmeOlay.Rows)
            {
                _ivmeGeneralModel = new IvmeGeneralModel();

                IvmeOlay _olay = new IvmeOlay();

                _olay.VAKAID = Convert.ToInt32(dr["VAKAID"]);
                _olay.LABORATUVARID = Convert.ToInt32(dr["LABORATUVARID"]);
                _olay.LABORATUVARKISAADI = dr["LABORATUVARKISAADI"].ToString();

                if (!string.IsNullOrEmpty(dr["ACILISTARIHI"].ToString()))
                {
                    _olay.ACILISTARIHI = (DateTime)dr["ACILISTARIHI"];
                }

                if (!string.IsNullOrEmpty(dr["KAPANISTARIHI"].ToString()))
                {
                    _olay.KAPANISTARIHI = (DateTime)dr["KAPANISTARIHI"];
                }


                if (!string.IsNullOrEmpty(dr["STATUSU"].ToString()))
                {
                    _olay.STATUSU = Convert.ToInt32(dr["STATUSU"]);
                }

                _olay.STATUADI = dr["STATUADI"].ToString();
                _olay.ULASIMYONTEMADI = dr["ULASIMYONTEMADI"].ToString();

                if (!string.IsNullOrEmpty(dr["OLAYILID"].ToString()))
                {
                    _olay.OLAYILID = Convert.ToInt32(dr["OLAYILID"]);
                }


                if (!string.IsNullOrEmpty(dr["OLAYILCEID"].ToString()))
                {
                    _olay.OLAYILCEID = Convert.ToInt32(dr["OLAYILCEID"]);
                }


                _olay.OLAYILADI = dr["OLAYILADI"].ToString();
                _olay.OLAYILCEADI = dr["OLAYILCEADI"].ToString();
                _olay.GONDERENKURUM = dr["GONDERENKURUM"].ToString();
                _olay.GONDERENMAKAM = dr["GONDERENMAKAM"].ToString();

                if (!string.IsNullOrEmpty(dr["GUNCELLENMETARIHI"].ToString()))
                {
                    _olay.GUNCELLENMETARIHI = (DateTime)dr["GUNCELLENMETARIHI"];
                }


                _ivmeGeneralModel.Olay = _olay;

                dataTableIvmeOlayTuru = new DataTable();

                dataTableIvmeOlayTuru = ExecuteSQLDataTable(IvmeOlayTuru(_olay.VAKAID));

                foreach (DataRow dr2 in dataTableIvmeOlayTuru.Rows)
                {
                    IvmeOlayTuru _ivmeOlayTuru = new IvmeOlayTuru();

                    _ivmeOlayTuru.VAKAID = Convert.ToInt32(dr2["VAKAID"]);

                    if (!string.IsNullOrEmpty(dr2["OLAYTURUID"].ToString()))
                    {
                        _ivmeOlayTuru.OLAYTURUID = Convert.ToInt32(dr2["OLAYTURUID"]);
                    }

                    _ivmeOlayTuru.OLAYTURUADI = dr2["OLAYTURUADI"].ToString();

                    if (!string.IsNullOrEmpty(dr2["OLAYTURUKODU"].ToString()))
                    {
                        _ivmeOlayTuru.OLAYTURUKODU = Convert.ToInt32(dr2["OLAYTURUKODU"]);
                    }


                    _ivmeGeneralModel.OlayTuru = new List<IvmeOlayTuru>();
                    _ivmeGeneralModel.OlayTuru.Add(_ivmeOlayTuru);
                }

                dataTableIvmeBulgu = new DataTable();

                dataTableIvmeBulgu = ExecuteSQLDataTable(IvmeBulgu(_olay.VAKAID));

                foreach (DataRow dr3 in dataTableIvmeBulgu.Rows)
                {
                    IvmeBulgu _ivmeOlayBulgu = new IvmeBulgu();

                    _ivmeOlayBulgu.BULGUID = Convert.ToInt32(dr3["BULGUID"]);
                    _ivmeOlayBulgu.VAKAID = Convert.ToInt32(dr3["VAKAID"]);
                    _ivmeOlayBulgu.BULGUSINIFID = Convert.ToInt32(dr3["BULGUSINIFID"]);
                    _ivmeOlayBulgu.BULGUTURID = Convert.ToInt32(dr3["BULGUTURID"]);
                    _ivmeOlayBulgu.BULGUSINIFADI = dr3["BULGUSINIFADI"].ToString();
                    _ivmeOlayBulgu.BULGUTURUADI = dr3["BULGUTURUADI"].ToString();

                    if (!string.IsNullOrEmpty(dr3["BULGUORNEKTIPID"].ToString()))
                    {
                        _ivmeOlayBulgu.BULGUORNEKTIPID = Convert.ToInt32(dr3["BULGUORNEKTIPID"]);
                    }


                    _ivmeOlayBulgu.BULGUORNEKTIPIADI = dr3["BULGUORNEKTIPIADI"].ToString();

                    if (!string.IsNullOrEmpty(dr3["BULGUDURUM"].ToString()))
                    {
                        _ivmeOlayBulgu.BULGUDURUM = Convert.ToInt32(dr3["BULGUDURUM"]);
                    }


                    _ivmeOlayBulgu.BULGUDURUMADI = dr3["BULGUDURUMADI"].ToString();

                    if (!string.IsNullOrEmpty(dr3["AMBALAJNITELIGI"].ToString()))
                    {
                        _ivmeOlayBulgu.AMBALAJNITELIGI = Convert.ToInt32(dr3["AMBALAJNITELIGI"]);
                    }

                    _ivmeOlayBulgu.AMBALAJNITELIGIADI = dr3["AMBALAJNITELIGIADI"].ToString();

                    if (!string.IsNullOrEmpty(dr3["BULGUSAYISI"].ToString()))
                    {
                        _ivmeOlayBulgu.BULGUSAYISI = Convert.ToInt32(dr3["BULGUSAYISI"]);
                    }


                    if (!string.IsNullOrEmpty(dr3["GUNCELLENMETARIHI"].ToString()))
                    {
                        _ivmeOlayBulgu.GUNCELLENMETARIHI = (DateTime)dr3["GUNCELLENMETARIHI"];
                    }


                    _ivmeGeneralModel.Bulgular = new List<IvmeBulgu>();
                    _ivmeGeneralModel.Bulgular.Add(_ivmeOlayBulgu);

                    _ivmeGeneralModels.Add(_ivmeGeneralModel);
                }
            }

            return _ivmeGeneralModels;
        }

        public Kullanici getKullanici(string _kodu, string _sifresi)
        {
            dataTableIvmeKullanici = new DataTable();

            dataTableIvmeKullanici = ExecuteSQLDataTable(IvmeKullanici(_kodu, _sifresi));

            if (dataTableIvmeKullanici == null)
            {
                return null;
            }

            Kullanici _kullanici = new Kullanici();

            _kullanici.KULLANICIID = Convert.ToInt32(dataTableIvmeKullanici.Rows[0]["KULLANICIID"]);
            _kullanici.KODU = _kodu;
            _kullanici.SIFRESI = _sifresi;

            return _kullanici;
        }

        public Kullanici getKullaniciById(int id)
        {
            dataTableIvmeKullanici = new DataTable();

            dataTableIvmeKullanici = ExecuteSQLDataTable(IvmeKullaniciById(id));

            if (dataTableIvmeKullanici == null)
            {
                return null;
            }

            Kullanici _kullanici = new Kullanici();

            _kullanici.KULLANICIID = Convert.ToInt32(id);
            _kullanici.KODU = dataTableIvmeKullanici.Rows[0]["KODU"].ToString();
            _kullanici.SIFRESI = dataTableIvmeKullanici.Rows[0]["SIFRESI"].ToString();

            return _kullanici;
        }

        #endregion

        #region DAO

        public DataTable ExecuteSQLDataTable(string strSQL)
        {
            string connectionString = _configuration.GetConnectionString("DEFAULT");

            SqlConnection con = new SqlConnection(connectionString);

            SqlCommand cmd = new SqlCommand(strSQL, con);

            SqlDataAdapter da = new SqlDataAdapter();

            da.SelectCommand = cmd;

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt;
        }

        #endregion

        #region Queries

        public string IvmeOlay(string date1, string date2)
        {
            string _sorgu = "SELECT  VAKAID , LABORATUVARID , LABORATUVARKISAADI , ACILISTARIHI , KAPANISTARIHI , STATUSU , STATUADI , ULASIMYONTEMADI , OLAYILID , " +
                "OLAYILCEID , OLAYILADI , OLAYILCEADI , GONDERENKURUM , GONDERENMAKAM , GUNCELLENMETARIHI FROM  LIAS_DEV.dbo.VW_IVME_OLAY WHERE KAPANISTARIHI >= '" + date1 + "' and " +
                "KAPANISTARIHI <= '" + date2 + "'";

            return _sorgu;
        }

        public string IvmeOlayTuru(int vakaId)
        {
            string _sorgu = "SELECT  VAKAID , OLAYTURUID , OLAYTURUADI , OLAYTURUKODU FROM  LIAS_DEV.dbo.VW_IVME_OLAYTURU WHERE VAKAID =" + vakaId.ToString();
            
            return _sorgu;
        }

        public string IvmeBulgu(int vakaId)
        {
            string _sorgu = "SELECT  BULGUID , VAKAID , BULGUSINIFID , BULGUTURID , BULGUSINIFADI , BULGUTURUADI , BULGUORNEKTIPID , BULGUORNEKTIPIADI , BULGUDURUM , BULGUDURUMADI , " +
                "AMBALAJNITELIGI , AMBALAJNITELIGIADI , BULGUSAYISI , GUNCELLENMETARIHI FROM  LIAS_DEV.dbo.VW_IVME_BULGU WHERE VAKAID =" + vakaId.ToString();

            return _sorgu;
        }

        public string IvmeKullanici(string kullaniciKodu, string sifre)
        {
            string _sorgu = "SELECT TOP 1 KULLANICIID FROM KULLANICI WHERE KODU = '" + kullaniciKodu + "' AND SIFRESI = '" + sifre + "'";

            return _sorgu;
        }

        public string IvmeKullaniciById(int id)
        {
            string _sorgu = "SELECT KODU, SIFRESI FROM KULLANICI WHERE KULLANICIID = " + id;

            return _sorgu;
        }


        #endregion


    }
}
