﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Entity
{
    public class IvmeOlayTuru
    {
        public int VAKAID { get; set; }
        public int OLAYTURUID { get; set; }
        public string OLAYTURUADI { get; set; }
        public int OLAYTURUKODU { get; set; }
    }
}
