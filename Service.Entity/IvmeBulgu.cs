﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Entity
{
    public class IvmeBulgu
    {
        public int BULGUID { get; set; }
        public int VAKAID { get; set; }
        public int BULGUSINIFID { get; set; }
        public int BULGUTURID { get; set; }
        public string BULGUSINIFADI { get; set; }
        public string BULGUTURUADI { get; set; }
        public int BULGUORNEKTIPID { get; set; }
        public string BULGUORNEKTIPIADI { get; set; }
        public int BULGUDURUM { get; set; }
        public string BULGUDURUMADI { get; set; }
        public int AMBALAJNITELIGI { get; set; }
        public string AMBALAJNITELIGIADI { get; set; }
        public int BULGUSAYISI { get; set; }
        public DateTime GUNCELLENMETARIHI { get; set; }

    }
}
