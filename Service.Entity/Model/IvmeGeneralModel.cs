﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Entity.Model
{
    public class IvmeGeneralModel
    {
        public IvmeOlay Olay { get; set; }
        public List<IvmeOlayTuru> OlayTuru  { get; set; }
        public List<IvmeBulgu> Bulgular { get; set; }
    }
}
