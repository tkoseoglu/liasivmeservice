﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Entity
{
    public class Kullanici
    {
        public decimal KULLANICIID { get; set; }
        public string KODU { get; set; }
        public string SIFRESI { get; set; }
    }
}
