﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Entity
{
    public class IvmeOlay
    {
        public int VAKAID { get; set; }
        public int LABORATUVARID { get; set; }
        public string LABORATUVARKISAADI { get; set; }
        public DateTime ACILISTARIHI { get; set; }
        public DateTime KAPANISTARIHI { get; set; }
        public int STATUSU { get; set; }
        public string STATUADI { get; set; }
        public string ULASIMYONTEMADI { get; set; }
        public int OLAYILID { get; set; }
        public int OLAYILCEID { get; set; }
        public string OLAYILADI { get; set; }
        public string OLAYILCEADI { get; set; }
        public string GONDERENKURUM { get; set; }
        public string GONDERENMAKAM { get; set; }
        public DateTime GUNCELLENMETARIHI { get; set; }
    }
}
